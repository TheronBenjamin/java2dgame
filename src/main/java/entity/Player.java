package entity;

import engine.GamePanel;
import engine.KeyHandler;
import engine.ScreenSize;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class Player extends Entity{

    GamePanel gamePanel;
    KeyHandler keyHandler;
    BufferedImage playerImage = null;


    public Player(GamePanel gamePanel, KeyHandler keyHandler) {
        this.gamePanel = gamePanel;
        this.keyHandler = keyHandler;
        setDefaultValues();
        getPlayerImage();
    }

    public void setDefaultValues(){
        x = 100;
        y = 100;
        speed = 4;
        direction = "down";
    }

    public void getPlayerImage(){
        try{
            up1 = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/player_walking/boy_up_1.png")));
            up2 = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/player_walking/boy_up_2.png")));
            down1 = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/player_walking/boy_down_1.png")));
            down2 = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/player_walking/boy_down_2.png")));
            left1 = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/player_walking/boy_left_1.png")));
            left2 = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/player_walking/boy_left_2.png")));
            right1 = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/player_walking/boy_right_1.png")));
            right2 = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/player_walking/boy_right_2.png")));
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    public void update(){

        if(keyHandler.upPressed|| keyHandler.downPressed || keyHandler.leftPressed || keyHandler.rightPressed) {
            if(keyHandler.upPressed) {
                direction = "up";
                y -= speed;
            }
            if(keyHandler.downPressed) {
                direction = "down";
                y += speed;
            }
            if(keyHandler.leftPressed) {
                direction = "left";
                x -= speed;
            }
            if(keyHandler.rightPressed) {
                direction = "right";
                x += speed;
            }
            if(x < 0) x = 0;
            if(x > ScreenSize.screenWidth - ScreenSize.tileSize) x = ScreenSize.screenWidth - ScreenSize.tileSize;
            if(y < 0) y = 0;
            if(y > ScreenSize.screenHeight - ScreenSize.tileSize) y = ScreenSize.screenHeight - ScreenSize.tileSize;

            movingSpriteWithFps();

        }

    }

    public void movingSpriteWithFps(){
        spriteCounter++;
        if(spriteCounter == 12){
            if(spriteNum == 1){
                spriteNum = 2;
            } else {
                spriteNum = 1;
            }
            spriteCounter = 0;
        }
    }

    public void drawComponent(Graphics g){

        switch (direction) {
            case "up" -> {
                if(spriteNum == 1) {
                    playerImage = up1;
                }
                if (spriteNum == 2) {
                    playerImage = up2;
                }
            }
            case "down" -> {
                if(spriteNum == 1) {
                    playerImage = down1;
                }
                if (spriteNum == 2) {
                    playerImage = down2;
                }
            }
            case "left" -> {
                if(spriteNum == 1) {
                    playerImage = left1;
                }
                if (spriteNum == 2) {
                    playerImage = left2;
                }
            }
            case "right" -> {
                if(spriteNum == 1) {
                    playerImage = right1;
                }
                if (spriteNum == 2) {
                    playerImage = right2;
                }
            }
        }
        g.drawImage(playerImage, x, y, ScreenSize.tileSize, ScreenSize.tileSize, null);
    }

}
