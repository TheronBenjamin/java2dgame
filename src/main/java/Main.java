import engine.GamePanel;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        //Generate the window
        JFrame window = new JFrame("Game");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);
        //Generate the game panel
        GamePanel gamePanel = new GamePanel();
        gamePanel.startGameThread();
        window.add(gamePanel);
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    }

}
