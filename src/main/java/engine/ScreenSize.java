package engine;

public final class ScreenSize {

    //SCREEN SETTINGS
    static final int originalTileSize = 16;
    static final int scale = 3;
    public static final int tileSize = originalTileSize * scale; //48x48 tile size
    static final int maxScreenCol = 16;
    static final int maxScreenRow = 12;
    public static final int screenWidth = maxScreenCol * tileSize; //768 pixels
    public static final int screenHeight = maxScreenRow * tileSize; //576 pixels

}
